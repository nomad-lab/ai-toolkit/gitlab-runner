FROM ubuntu:22.04

# ================================================================================
# Linux applications and libraries
# ================================================================================

RUN apt update --yes \
 && apt install --yes --quiet --no-install-recommends \
    openssh-client \
 && apt clean \
 && rm -rf /var/lib/apt/lists/*

